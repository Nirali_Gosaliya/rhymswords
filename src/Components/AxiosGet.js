import React, { useEffect } from 'react'
import axios from 'axios'

function AxiosGet() {

    useEffect(()=>{
        axios.get('https://jsonplaceholder.typicode.com/todos')
        .then(response=>console.log(response.data))
    },[])
  return (
    <div>
        <h2>axios get method</h2>
    </div>
  )
}

export default AxiosGet;