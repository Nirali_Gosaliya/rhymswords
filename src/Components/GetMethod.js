import React,{useState,useEffect} from 'react'

function GetMethod() {
  
    const [name,setName] = useState("")

    useEffect(()=>{
        async function fetchData  ()  {
            await fetch('https://jsonplaceholder.typicode.com/posts/1' ,{
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify()
            }).then(response=>setName(console.log(response)))
    
        }
        fetchData();
    },[])
        

  return (
    <div>
       
    </div>
  )
}

export default GetMethod;