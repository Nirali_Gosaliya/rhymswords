

import React from 'react'
import axios from 'axios'

function AxiosPost() {
   

    const handleSubmit = () => {
      

        axios.post('https://jsonplaceholder.typicode.com/todos',{
            title:"Hello World",
            completed:true
    })
        .then(response=>console.log(response.data))
        .catch(error=>console.log(error))
    }
  return (
    <div>
         
            
            <button type="submit" onClick={handleSubmit}>Submit</button>
         
          
    </div>
  )
}

export default AxiosPost;