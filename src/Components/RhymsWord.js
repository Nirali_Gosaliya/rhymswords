import React,{useState} from 'react'
//  "Write a small React JS web application that prompts the user to enter a word, and in response provides a list of words that rhyme with that word. To find which words rhyme you may use an API, for example https://www.datamuse.com/api/ or any API of your choice. 

function RhymsWord() {
    const [user,setUser] = useState("");
    const [ans,setAns] = useState([])

    const handleSubmit = () => {
        fetch(`https://api.datamuse.com/words?rel_rhy=${user}`)
        .then(response=>response.json())
        .then(result=>setAns(result))
    }
  return (
    <div>
       <div>
       <input type="text" onChange={(e)=>setUser(e.target.value)} value={user}></input>
        <button type="submit" onClick={handleSubmit}>Submit</button>
       
       {ans.map((item,index)=>{
           return <h2 key={index}>{item.word}</h2>

       })}
       </div>
    </div>
  )
}

export default RhymsWord;