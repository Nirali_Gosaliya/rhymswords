
import './App.css';
import GetMethod from './Components/GetMethod';
import AxiosGet from './Components/AxiosGet';
import AxiosPost from './Components/AxiosPost';
import RhymsWord from './Components/RhymsWord';
import AsyncAwaitAxiosPost from './Components/AsyncAwaitAxiosPost';


function App() {
  return (
    <div className="App">
     {/* <GetMethod /> */}
     {/* <AxiosGet /> */}
     {/* <AxiosPost /> */}
     {/* <RhymsWord /> */}
     {/* <AsyncAwaitAxiosPost /> */}
    </div>
  );
}

export default App;
